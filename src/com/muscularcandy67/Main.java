package com.muscularcandy67;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) {
        String word;
        StringTokenizer wordst;
        try (Scanner keyboard = new Scanner(System.in)) {
            word = keyboard.next();
        }
        wordst = new StringTokenizer(word, ":");
        while (wordst.hasMoreTokens()) System.out.println(wordst.nextToken());
    }
}
